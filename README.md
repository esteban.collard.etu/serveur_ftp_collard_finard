# Serveur_FTP_Collard_Finard
Collard Esteban Finard Alexis   
09/02/2021

## Introduction

L'objectif de ce projet est de mettre en oeuvre un serveur conforme au protocole applicatif [FTP](https://fr.wikipedia.org/wiki/File_Transfer_Protocol). Ce serveur doit permettre le stockage et l'envoie des fichiers en respectant le standard FTP.

#### Compilation et exécution

La commande `mvn package` permet de compiler le projet.   
La commande `java -jar target/Serveur_FTP_Collard_Finard-Serveur_FTP_Collard_Finard.jar` permet d'exécuter le projet.   
   
L'exécutable peut prendre plusieurs arguments en paramètre. Il est notamment possible de fournir :   
- Le dossier qui servira de répertoire racine (facultatif)
- Le numéro de port du serveur ftp (facultatif)

Si aucun argument n'est fourni, le répertoire racine sera le répertoire `dossiertest/` et le port sera le numéro `8080`.   

Informations : les informations de connexion sont disponibles dans le fichier `fileLog.txt`

Quelques exemples d'utilisations:   

`java -jar target/Serveur_FTP_Collard_Finard-Serveur_FTP_Collard_Finard.jar`   
`java -jar target/Serveur_FTP_Collard_Finard-Serveur_FTP_Collard_Finard.jar dossierTest/`   
`java -jar target/Serveur_FTP_Collard_Finard-Serveur_FTP_Collard_Finard.jar dossierTest/ 8080`   

#### Démonstration video

![Démonstration vidéo](doc/video.gif)


## Architecture

#### Diagramme de classe

Voici le diagramme de classe représentant le projet

![Diagramme de classe](doc/Diagramme_de_classe.png)   

La classe principale est `ServeurFtp`, elle possède la méthode `main` lui permettant de créer un serveur en fonction des arguments fournis. Un serveur possède de nombreux attributs comme par exemple un `ServeurSocket`, un `Socket`, une liste de `ClientFtp`. Ses méthodes sont elles aussi nombreuses, il y a par exemple une méthode `SendMessage`, une méthode `readMessage` ou encore `removeClient`.   

La seconde classe la plus importante est `ClientFtp`. Un client possède un grand nombre d'attributs, par exemple, un `Socket`, une chaine de caractère `repertoireCourant`, des booléens `isConnected` et `isLog`, ou encore son `ServeurFtp` associé.   

Les autres classes représentent les différentes requêtes possibles pour un client. Elles possèdent une méthode `executer` qui est utilisée par `ClientFtp`
#### Gestion d'erreur
Dans notre programme, de nombreuses erreurs peuvent survenir lorsqu'une requête est faite par un client. Si une erreur se présente, alors la réponse à cette requête est un code de réponse négatif.

```java
try {
    // Execution de la commande cd et ls
    Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", changeDirectory+" \n"+lscmd});
    p.waitFor();
    // Lecture du resultat de la commande ls dans le fichier /tmp/commande.txt
    Scanner myReader = new Scanner(new File("/tmp/commande.txt"));
    // .....
}
catch (IOException | InterruptedException e) {
    c.sendMessage("550 failed to print directory");
}
```
## Code Samples

#### Du code en une ligne

Cet exemple est intéressant car il décompose le travail en deux méthodes privées et ne tiens que sur une seule ligne, le rendant facile à comprendre. 

```java
public static String executer(ClientFtp clientFtp, String string) {
    return (string.endsWith(".txt") ? retrTxt(clientFtp,string) : retrBin(clientFtp,string));
}
```
#### Une astuce pour la déconnexion d'un client

Afin de savoir si un client est déconnecté, on par du principe que la méthode `readMessage` ne sera plus bloquante et renverra alors une chaine vide.
```java
while(this.isConnected) {
    msg = readMessage();
    if(msg!=null) {
        if(isLog) {
            traitementMsg(msg);					
        }
        else {
            traitementMsgLog(msg);
        }
    }
    else {
        this.isConnected = false;
    }
}
```
#### Le traitement d'une requête

Le code pour traiter une requête d'un client consiste à récupérer le premier mot et à faire un `switch/case` sur toutes nos requêtes implémentées.
```java
private void traitementMsg(String msg) {
    String[] msgSplit = msg.split(" ");
    switch(msgSplit[0]) {
        case "PWD":
            this.sendMessage(RequetePwd.executer(this));
            break;
        case "SYST":
            this.sendMessage(RequeteSyst.executer());
            break;
        case "TYPE":
            this.sendMessage(RequeteType.executer());
            break;
        case "PASV":
            this.sendMessage(RequetePasv.executer(this));
            break;
        //...
        //...
	//...
```

#### La commande List
Afin de connaitre le répertoire d'un dossier, on exécute la commande `ls`, puis on écrit le résultat dans un fichier temporaire (dans `/tmp/`).
```java
public static String executer(ClientFtp c) {
    String changeDirectory = "cd "+ServeurFtp.getRepertoireCourant()+c.getRepertoireCourant();
    String lscmd = "ls -al > /tmp/commande.txt";
    try {
        // Execution de la commande cd et ls
        Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", changeDirectory+" \n"+lscmd});
        p.waitFor();
	//...
	//...
	//...
```

## Objectif réalisé

- CMP:  OK
- DOC:  OK
- EXE:  OK
- CON:  OK
- BADU: OK
- BADP: OK
- LST:  OK
- CWD:  OK
- CDUP: OK
- ROOT: OK
- GETT: OK
- GETB: OK
- GETR: OK
- PUTT: OK
- PUTB: OK
- PUTR: KO
- RENF: OK
- MKD:  OK
- REND: OK
- RMD:  OK
- CLOS: OK
- PORT: OK
- HOME: OK
- ACPA: KO (debut d'implémentation)
- THRE: OK
