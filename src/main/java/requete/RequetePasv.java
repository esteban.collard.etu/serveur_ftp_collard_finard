package requete;

import java.net.InetAddress;
import java.net.UnknownHostException;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande PASV
 * @author Collard Finard
 *
 */
public class RequetePasv {
	
	/**
	 * creer un dossier
	 * @param c ClientFtp : client qui veut recuperer des ports de connexions
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c) {
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();

			int port;
			boolean portTrouver = false;

			for(port=10000;(port<=65635 && !portTrouver);) {
				port++;
				if(ServeurFtp.portCanBeUsed(port)) {
					portTrouver=true;
					
					//commentaire a enlever si vous souhaiter tester
					//ligne enlever car sinon la list des ports du serveurs va se remplir pour rien
					//ServeurFtp.addPort(port);
				}
			}

			if(portTrouver) {
				int n5 = (int)port/256;
				int n6 = port%256;
				c.setAdresseTransfert(ip.getHostAddress(), n5*256+n6);
				//return "220 Entering Passive Mode ("+n1+","+n2+","+n3+","+n4+","+n5+","+n6+")";
				return "220 Entering Passive Mode ("+ip.getHostAddress()+"."+n5+"."+n6+")";
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return "530 Impossible de recuperer adresse";
	}
}
