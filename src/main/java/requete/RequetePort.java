package requete;

import clientFtp.ClientFtp;

/**
 * classe gerant la commande PORT
 * @author Collard Finard
 *
 */
public class RequetePort {
	
	/**
	 * Recuperer l'adresse ip et le port envoyer par le client, et le sauvegarde
	 * @param c ClientFtp : client qui envoie le port de connexion
	 * @param numAdresse String: le port envoyer
	 * @return
	 */
	public static String executer(ClientFtp c, String numAdresse) {
		String[] allNumAdresse = numAdresse.split(",");
		
		c.setAdresseTransfert(allNumAdresse[0]+"."+allNumAdresse[1]+"."+allNumAdresse[2]+"."+allNumAdresse[3],Integer.parseInt(allNumAdresse[4])*256+Integer.parseInt(allNumAdresse[5]));
		
		return "200 Adresse OK";
	}
}
