package requete;

import java.io.File;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande RNTO
 * @author Collard Finard
 *
 */
public class RequeteRnto {

	/**
	 * commande permetant de renomer un fichier ou un dossier
	 * @param clientFtp ClientFtp : client qui veut renomer un ficheir
	 * @param newNameFile String : nouveau nom du fichier ou dossier
	 * @return
	 */
	public static String executer(ClientFtp clientFtp, String newNameFile) {
		if(!clientFtp.getFileToRename().equals("")) {
			File file = new File(ServeurFtp.getRepertoireCourant(), clientFtp.getFileToRename());
			File file2 = new File(ServeurFtp.getRepertoireCourant()+"/"+clientFtp.getRepertoireCourant(), newNameFile);
			clientFtp.setFileToRename("");
			boolean success = file.renameTo(file2);

			if (success) {
			   return "250 rename OK";
			}
			else {
				return "550 echec du rename";				
			}
		}
		else {
			return "550 probleme nom";
		}

	}

}
