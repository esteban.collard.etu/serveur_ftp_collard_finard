package requete;

import java.io.File;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande MKD
 * @author Collard Finard
 *
 */
public class RequeteMkd {
	
	/**
	 * creer un dossier
	 * @param c ClientFtp : client qui veut creer un dossier
	 * @param nameDossier String : nom de dossier a creer
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c, String nameDossier) {
		String cheminDossier = ServeurFtp.getRepertoireCourant()+c.getRepertoireCourant()+"/"+nameDossier;

		File dossier = new File(cheminDossier); 
		boolean res = dossier.mkdir();

		if(res) {
			return "257 Dossier correctement creer";
		}
		else {
			return "550 probleme non dossier existe deja";
		}
	}
}