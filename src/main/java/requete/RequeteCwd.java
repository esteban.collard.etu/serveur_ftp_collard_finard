package requete;

import clientFtp.ClientFtp;

/**
 * classe gerant la commande CWD
 * @author Collard Finard
 *
 */
public class RequeteCwd {
	
	/**
	 * Ouvre un dossier
	 * @param c ClientFtp : client qui veut ce deplacer dans un dossier
	 * @param repertoire String : repertoire a ouvrir
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c, String repertoire) {
					
		if(repertoire.equals("..")) {
			String[] repertoireCourant = c.getRepertoireCourant().split("/");
			if(repertoireCourant.length>0) {
				String newRepertoireCourant = "";
				for(int i=0;i<repertoireCourant.length-1;i++) {
					newRepertoireCourant+="/"+repertoireCourant[i];
				}
				c.setRepertoireCourant(newRepertoireCourant);
				return "250 Directory successfully changed.";
			}
			else {
				c.setRepertoireCourant("/");
				return "257 \"/\" is the current directory.";
			}
		}
		else {
			if(repertoire.charAt(0) == '/') {
				c.setRepertoireCourant(repertoire);
				return "250 Directory successfully changed.";
			}
			else {
				String repertoireCourant = c.getRepertoireCourant();
				
				if(c.getRepertoireCourant().equals("/")) {
					c.setRepertoireCourant(repertoireCourant+repertoire);
				}
				else {				
					c.setRepertoireCourant(repertoireCourant+"/"+repertoire);				
				}
				return "250 Directory successfully changed.";
			}
		}	
	}
}
