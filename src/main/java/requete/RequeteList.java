package requete;


import java.util.Scanner;
import java.io.File;
import java.io.IOException;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande LIST
 * @author Collard Finard
 *
 */
public class RequeteList {

	/**
	 * envoi la liste des fichiers du repertoire du client
	 * @param c ClientFtp : client qui veut afficher le repertoire
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c) {
		String changeDirectory = "cd "+ServeurFtp.getRepertoireCourant()+c.getRepertoireCourant();
		String lscmd = "ls -al > /tmp/commande.txt";
		try {
			// Execution de la commande cd et ls
			Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", changeDirectory+" \n"+lscmd});
			p.waitFor();
			
			// Lecture du resultat de la commande ls dans le fichier /tmp/commande.txt
			Scanner myReader = new Scanner(new File("/tmp/commande.txt"));
			c.sendMessageCanalDonnee(myReader.useDelimiter("\\Z").next()+"\n"); // "\\Z" est la fin de la chaine
			c.sendMessage("226 send ok");
			myReader.close();
		}
		catch (IOException | InterruptedException e) {
			c.sendMessage("550 failed to print directory");
		}
		return null;
	}
}
