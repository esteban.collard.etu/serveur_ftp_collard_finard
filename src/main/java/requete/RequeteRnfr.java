package requete;

import java.io.File;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande RNFR
 * @author Collard Finard
 *
 */
public class RequeteRnfr {
	
	/**
	 * renomme un fichier
	 * @param c ClientFtp : client qui veut renomer un fichier
	 * @param string String : nom du fichier a renommer
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c, String string) {
		File f = new File(ServeurFtp.getRepertoireCourant()+c.getRepertoireCourant(), string);
		if(f.isFile() || f.isDirectory())
		{
			c.setFileToRename(c.getRepertoireCourant()+"/"+string);
			return "331 SVP specifier nouveau nom";
		}
		else {
			return "550 probleme nom";
		}
	}
}
