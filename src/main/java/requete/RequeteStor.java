package requete;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande STOR
 * @author Collard Finard
 *
 */
public class RequeteStor {
	
	/**
	 * envoi un fichier au serveur ftp
	 * @param c ClientFtp : client qui veut renomer un fichier
	 * @param nameDossier : nom du fichier a telecharger
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c, String nameDossier) {
		File newFile = new File(ServeurFtp.getRepertoireCourant()+c.getRepertoireCourant(), nameDossier);
		if(!newFile.exists()) {
			try {
				c.sendMessage("200 pret a recevoir.");
				if(newFile.createNewFile()) {
					FileWriter myWriter = new FileWriter(newFile);
				    myWriter.write(c.getStringCanalDonnee());
				    myWriter.close();
					return "110 upload OK";
				}
				else {
					return "500 Impossible de creer le fichier";
				}
			} catch (IOException e) {
				return "500 Impossible de creer le fichier";
			}
		}
		else {
			return "553 le fichier existe deja";
		}
	}
}
