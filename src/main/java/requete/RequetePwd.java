package requete;

import clientFtp.ClientFtp;

/**
 * classe gerant la commande PWD
 * @author Collard Finard
 *
 */
public class RequetePwd {
	
	/**
	 * retourne le nom du repertoire actuel
	 * @param c ClientFtp : client qui veut connaitre sont repertoire actuel
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp c) {
		return "257 \""+c.getRepertoireCourant()+"\" is the current directory";
	}
}
