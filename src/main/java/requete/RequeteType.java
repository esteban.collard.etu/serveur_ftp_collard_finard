package requete;

/**
 * classe gerant la commande TYPE
 * @author Collard Finard
 *
 */
public class RequeteType {
	/**
	 * Defini le mode de transfert ASCII. 
	 * @return String : message retourner par le serveur
	 */
	public static String executer() {
		return "200 A\n";
	}
}
