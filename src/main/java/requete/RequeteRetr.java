package requete;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande RETR
 * @author Collard Finard
 *
 */
public class RequeteRetr {

	/**
	 * Envoi un fichier en binaire ou en ascii selon le type
	 * @param clientFtp ClientFtp : client qui veut telecharger le fichier
	 * @param string String : nom du fichier a telecharger
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp clientFtp, String string) {
		return (string.endsWith(".txt") ? retrTxt(clientFtp,string) : retrBin(clientFtp,string));
	}

	/**
	 * Envoi un fichier en binaire
	 * @param clientFtp ClientFtp : client qui veut telecharger le fichier
	 * @param string String : nom du fichier a telecharger
	 * @return String : message retourner par le serveur
	 */
	private static String retrBin(ClientFtp clientFtp, String string) {
		byte[] bytes;
		try {
			bytes = Files.readAllBytes(Paths.get(ServeurFtp.getRepertoireCourant()+ clientFtp.getRepertoireCourant()+"/" +string));
			clientFtp.sendMessage("150 Opening BINARY mode data connection for "+ clientFtp.getRepertoireCourant() +"/" +string);
			clientFtp.sendBinaryCanalDonnee(bytes);
			clientFtp.sendMessage("226 Transfer complete.");

		} catch (IOException e) {
			clientFtp.sendMessage("502 echec du telechargement\n");
		}
		return null;

	}
	
	/**
	 * Envoi un fichier ascii selon le type
	 * @param clientFtp ClientFtp : client qui veut telecharger le fichier
	 * @param string String : nom du fichier a telecharger
	 * @return String : message retourner par le serveur
	 */
	private static String retrTxt(ClientFtp clientFtp, String string) {
		Scanner myReader;
		String valRet;
		try {
			myReader = new Scanner(new File(ServeurFtp.getRepertoireCourant()+ clientFtp.getRepertoireCourant()+"/" +string));
			clientFtp.sendMessage("150 Opening TEXT mode data connection for " + string);
			myReader.useDelimiter("\\Z"); // le délimiteur est la fin du fichier
			valRet = (myReader.hasNext() ? myReader.next()+"\n" : ""); 
			clientFtp.sendMessageCanalDonnee(valRet);
			clientFtp.sendMessage("226 Transfer complete.");
			myReader.close();
		} catch (FileNotFoundException e) {
			clientFtp.sendMessage("502 Fichier introuvable\n");
		}
		return null;
	}
}
