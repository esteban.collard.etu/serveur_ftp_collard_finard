package requete;

import java.io.File;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande DELE
 * @author Collard Finard
 *
 */
public class RequeteDele {

	/**
	 * supprime un fichier
	 * @param clientFtp ClientFtp : client qui veut supprimer un client
	 * @param nameFichier String : fichier a supprimer
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp clientFtp, String nameFichier) {
		String cheminFichier = ServeurFtp.getRepertoireCourant()+clientFtp.getRepertoireCourant()+"/"+nameFichier;

		File fichier = new File(cheminFichier);
		if(fichier.isFile()) {
			boolean res = fichier.delete();
			
			if(res) {
				return "257 Fichier correctement supprimer";
			}
			else {
				return "550 probleme le fichier n'a pas pu etre supprimer";
			}			
		}
		else {
			return "500 probleme ce n'est pas un fichier";
		}
	}

}
