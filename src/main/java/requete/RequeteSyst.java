package requete;

/**
 * classe gerant la commande SYST
 * @author Collard Finard
 *
 */
public class RequeteSyst {
	
	/**
	 * affiche le system du serveur
	 * @return String : message retourner par le serveur
	 */
	public static String executer() {
		return "215 SRV Collard Finard\n";
	}
}
