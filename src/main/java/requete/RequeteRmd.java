package requete;

import java.io.File;

import clientFtp.ClientFtp;
import serveurFtp.ServeurFtp;

/**
 * classe gerant la commande RMD
 * @author Collard Finard
 *
 */
public class RequeteRmd {

	/**
	 * supprime un dossier du serveur
	 * @param clientFtp ClientFtp : client qui veut supprimer un dossier
	 * @param nameDossier String : nom du dossier a supprimer
	 * @return String : message retourner par le serveur
	 */
	public static String executer(ClientFtp clientFtp, String nameDossier) {
		String cheminDossier = ServeurFtp.getRepertoireCourant()+clientFtp.getRepertoireCourant()+"/"+nameDossier;
		File dossier = new File(cheminDossier);
		if(dossier.isDirectory()) {
			boolean res = dossier.delete();
			
			if(res) {
				return "257 Dossier correctement supprimer";
			}
			else {
				return "550 probleme le dossier n'a pas pu etre supprimer";
			}			
		}
		else {
			return "500 probleme ce n'est pas un dossier";
		}
	}
}
