package serveurFtp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import clientFtp.*;

/**
 * représente un serveur Ftp
 * @author Collard finard
 * 
 */
public class ServeurFtp {
	
	/**
	 * le ServeurSocket utilisé par la classe ServeurFtp
	 */
	private ServerSocket serverSocket;
	
	/*
	 * la liste des clients connectés sur le serveur
	 */
	private List<ClientFtp> listClient;
	
	/*
	 * un socket utilisé par un client 
	 */
	private Socket socket;
	
	/*
	 * une hashmap représentant le lien entre identifiant et mot de passe
	 */
	private static HashMap<String, String> verifConnection;
	
	/*
	 * la liste des identifiants connectés
	 */
	private static List<String> listClientLog;
	
	/*
	 * la nom du repertoire racine
	 */
	private static String rootDir;
	
	/*
	 * le numéro de port
	 */
	private static int portFtp;	
	
	/*
	 * la liste des ports utilisé
	 */
	private static List<Integer> listPortUtiliser;
	
	/**
	 * Créer un Serveur et attends la connexion de client
	 * @param portNumber int : le numéro de port du serveur
	 */
	public ServeurFtp(int portNumber) {
		this.listClient = new ArrayList<ClientFtp>();
		listClientLog = new ArrayList<String>();
		verifConnection = new HashMap<String,String>();
		listPortUtiliser = new ArrayList<Integer>();
		listPortUtiliser.add(portNumber);
		
		fileLogConnect();

		try {
			// Serveur connecté
			this.serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			e.printStackTrace();
		}

		while (true) {
			System.out.print("nbClient = "+this.listClient.size()+"\n");
			try {
				socket = serverSocket.accept();
			} catch (IOException e) {
				System.out.println("I/O error: " + e);
			}
			// new thread for a client
			ClientFtp tmpClient = new ClientFtp(socket,this);
			tmpClient.start();
			this.listClient.add(tmpClient);

		}
	}
	
	/**
	 * vérifie si un port peut être utilisé
	 * @param portTest Integer : le numéro de port
	 * @return boolean : true si le port peut être utilisé
	 */
	public static boolean portCanBeUsed(Integer portTest){
		return !listPortUtiliser.contains(portTest);
	}
	
	/**
	 * ajoute un port à la liste des port utilisé
	 * @param portTest Integer : un numéro de port
	 */
	public static void addPort(Integer portTest){
		listPortUtiliser.add(portTest);
	}
	
	/**
	 * enlève un port de la liste des port utilisé
	 * @param port Integer : un numéro de port
	 */
	public static void removePort(Integer port){
		if(listPortUtiliser.contains(port)) {
			listPortUtiliser.remove(port);
		}
	}
	
	
	/**
	 * 
	 * @param args String[] : les arguments passés en paramètre
	 */
	public static void main (String[] args){
		if (args.length == 1) {
			rootDir = args[0];
			portFtp = 8080;
		}
		else if(args.length == 2) {
			rootDir = args[0];
			portFtp = Integer.parseInt(args[1]);
		}
		else {
			rootDir = "./dossierTest";
			portFtp = 8080;
		}
		ServeurFtp s = new ServeurFtp(portFtp);
	}
	
	/**
	 * envoie un message à un client
	 * @param c ClientFtp : le client
	 * @param message String : le message
	 */
	public void sendMessageClient(ClientFtp c, String message) {
		c.sendMessage(message);
	}
	
	/**
	 * récupère les identifiants/mot de passe du fichier fileLog.txt
	 */
	private void fileLogConnect() {
		BufferedReader lecteurAvecBuffer = null;
		String ligne;

		try
		{
			lecteurAvecBuffer = new BufferedReader(new FileReader("./fileLog.txt"));
		}
		catch(FileNotFoundException exc)
		{
			System.out.println("Erreur d'ouverture");
		}
		
		try {
			while ((ligne = lecteurAvecBuffer.readLine()) != null) {
				String[] tabLigne = ligne.split(":");
				if(tabLigne.length==2) {
					verifConnection.put(tabLigne[0], tabLigne[1]);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(lecteurAvecBuffer != null) {
			try {
				lecteurAvecBuffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * lis un message provenant d'un client
	 * @param c ClientFtp : le client
	 * @return String : le message
	 */
	public String readMessageClient(ClientFtp c) {
		return c.readMessage();
	}
	
	/**
	 * vérifie si il est possible de s'identifier
	 * @param user String : le nom d'utilisateur
	 * @param pass String : le mot de passe
	 * @return Boolean : true si la connection est possible
	 */
	public static Boolean canConnect(String user, String pass) {
		return (verifConnection.containsKey(user) && verifConnection.get(user).equals(pass) && !listClientLog.contains(user));
	}
	
	/**
	 * donne le répertoire courant
	 * @return String : le répertoire courant
	 */
	public static String getRepertoireCourant() {
		return rootDir;
	}
	
	/**
	 * enlève un client de la liste des clients
	 * @param clientFtp ClientFtp : le client à retirer
	 */
	public void removeClient(ClientFtp clientFtp) {
		this.listClient.remove(clientFtp);
	}

}
