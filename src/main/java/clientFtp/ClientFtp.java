package clientFtp;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import requete.*;
import serveurFtp.ServeurFtp;

/**
 * represente un client FTP
 * @author Collard Finard
 *
 */
public class ClientFtp extends Thread  {

	/**
	 * nom d'utilisateur du client ftp
	 */
	private String user;
	
	/**
	 * mot de passe du client ftp
	 */
	private String password;
	
	/**
	 * repertoire ou se situe le client ftp
	 */
	private String repertoireCourant;

	/**
	 * socket etablissant la connexion principal entre le client et le serveur
	 */
	private Socket mySocket;
	
	/**
	 *  variable pour lire les messages du client (connexion principale)
	 */
	private BufferedReader myReader;
	
	/**
	 *  variable pour envoyer des messages au client (connexion principale)
	 */
	private BufferedWriter myWriter;

	/**
	 * valeur boolean qui dit si le client est connecter au client (ne veux pas dire qu'il c'est identifier)
	 */
	private Boolean isConnected;
	
	/**
	 * valeur boolean qui dit si le client c'est identifier
	 */
	private Boolean isLog;

	/**
	 * stock le port utiliser pour la connexion secondaire (canal de donnee)
	 */
	private int portCanalDonnees;
	
	/**
	 * stock l'adresse ip pour la connexion secondaire (canal de donnee)
	 */
	private String serveurCanalDonnees;
	
	/**
	 * nom du fichier a renomer
	 */
	private String fileToRename;

	/**
	 * serveur ftp ou c'est connecter le client
	 */
	private ServeurFtp serveurFtp;
	
	/**
	 * creer un client ftp
	 * @param clientSocket Socket : socket utiliser pour la connexion
	 * @param serveurFtp ServeurFtp : serveur creant le client
	 */
	public ClientFtp(Socket clientSocket, ServeurFtp serveurFtp){

		try {
			this.serveurFtp = serveurFtp;
			this.mySocket = clientSocket;
			this.myReader = new BufferedReader(new InputStreamReader(this.mySocket.getInputStream()));
			this.myWriter = new BufferedWriter(new OutputStreamWriter(this.mySocket.getOutputStream()));
			this.isConnected = true;
			this.isLog = false;
			this.repertoireCourant = "/";
			this.fileToRename = "";

			this.sendMessage("220 Welcome to Collard Finard srv");
		}
		catch (IOException e) {
			e.printStackTrace();
			this.isConnected = false;
		}
	}

	/**
	 * envoye un message au client via la connexion principal
	 * @param message String : message envoyer
	 */
	public void sendMessage(String message) {
		try {
			this.myWriter.write(message + "\r\n");
			this.myWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * lit un message du client via la connexion principal
	 * @return String : message envoyer par le client
	 */
	public String readMessage() {
		try {
			return this.myReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			this.isConnected=false;
			return "error";
		}
	}

	/**
	 * retourne le nom d'utilisateur
	 * @return String : nom d'utilisateur
	 */
	public String getUser() {
		return this.user;
	}

	/**
	 * Modifie le repertoire courant
	 * @param repertoire String : repertoire courant a modifier
	 */
	public void setRepertoireCourant(String repertoire) {
		this.repertoireCourant = repertoire;
	}

	/**
	 * retourne le repertoire courant
	 * @return String : le repertoire courant
	 */
	public String getRepertoireCourant() {
		return this.repertoireCourant;
	}

	/**
	 * thread gerant la connexion entre le client et le serveur
	 */
	public void run() {
		String msg;
		while(this.isConnected) {
			msg = readMessage();
			if(msg!=null) {
				if(isLog) {
					traitementMsg(msg);					
				}
				else {
					traitementMsgLog(msg);
				}
			}
			else {
				this.isConnected = false;
			}
		}
		try {
			this.myReader.close();
			this.myWriter.close();
			this.mySocket.close();
			this.serveurFtp.removeClient(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * traite les messages du client si il est identifie
	 * @param msg String : message envoyer par le client
	 */
	private void traitementMsg(String msg) {
		String[] msgSplit = msg.split(" ");
		switch(msgSplit[0]) {
		case "PWD":
			this.sendMessage(RequetePwd.executer(this));
			break;
		case "SYST":
			this.sendMessage(RequeteSyst.executer());
			break;
		case "TYPE":
			this.sendMessage(RequeteType.executer());
			break;
		case "PASV":
			this.sendMessage(RequetePasv.executer(this));
			break;
		case "PORT":
			this.sendMessage(RequetePort.executer(this, msgSplit[1]));
			break;
		case "LIST":
			RequeteList.executer(this);
			break;
		case "CWD":
			this.sendMessage(RequeteCwd.executer(this, msgSplit[1]));
			break;
		case "QUIT":
			this.isConnected=false;
			break;
		case "RETR":
			RequeteRetr.executer(this,msgSplit[1]);
			break;
		case "RNFR":
			this.sendMessage(RequeteRnfr.executer(this, msgSplit[1]));
			break;
		case "RNTO":
			this.sendMessage(RequeteRnto.executer(this, msgSplit[1]));
			break;
		case "MKD":
			this.sendMessage(RequeteMkd.executer(this,msgSplit[1]));
			break;
		case "DELE":
			this.sendMessage(RequeteDele.executer(this,msgSplit[1]));
			break;
		case "RMD":
			this.sendMessage(RequeteRmd.executer(this,msgSplit[1]));
			break;
		case "STOR":
			this.sendMessage(RequeteStor.executer(this,msgSplit[1]));
			break;
		default:
			this.sendMessage("502 Commande non implementee.");
		}
	}

	/**
	 * traite les messages du client si il n'est pas identifer
	 * @param msg String : message envoyer par le client
	 */
	private void traitementMsgLog(String msg) {
		String[] msgSplit = msg.split(" ");
		switch(msgSplit[0]) {
		case "USER":
			this.user = msgSplit[1];
			this.sendMessage("331 SVP spécifier mot de passe.");
			break;
		case "PASS":
			this.password = msgSplit[1];
			if(logIsCorrect()) {
				this.sendMessage("230 Connection réussi.");				
			}
			else {
				this.sendMessage("530 Connexion non établie.");
			}
			break;
		default:
			this.sendMessage("530 SVP connectez vous avec USER et PASS.");
		}
	}

	/**
	 * Verifie si les identifiant du client sont correctes
	 * @return true si les identfiants sont correcte, false sinon
	 */
	private boolean logIsCorrect() {
		this.isLog = ServeurFtp.canConnect(this.user, this.password);
		return this.isLog;
	}

	/**
	 * Change le port et l'adresse ip utilise pour la connextions utiliser
	 * @param serveurPortCanal
	 * @param numPortCanal
	 */
	public void setAdresseTransfert(String serveurPortCanal, int numPortCanal) {
		this.portCanalDonnees = numPortCanal;
		this.serveurCanalDonnees =serveurPortCanal;
	}

	/**
	 * envoi un message sur le canal secondaire
	 * @param msg String : message a envoyer
	 */
	public void sendMessageCanalDonnee(String msg) {

		Socket socketTmp;
		try {
			socketTmp = (new Socket(this.serveurCanalDonnees, this.portCanalDonnees));
			BufferedWriter myWriterTmp = new BufferedWriter(new OutputStreamWriter(socketTmp.getOutputStream()));
			myWriterTmp.write(msg);
			myWriterTmp.close();
			socketTmp.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}
	
	/**
	 * envoi un message sur le canal secondaire
	 * @param msg bytes : message a envoyer
	 */
	public void sendBinaryCanalDonnee( byte[] bytes) {
		Socket socketTmp;
		try {
			socketTmp = (new Socket(serveurCanalDonnees, portCanalDonnees));
			DataOutputStream myWriterTmp = new DataOutputStream(socketTmp.getOutputStream());
			myWriterTmp.write(bytes);
			myWriterTmp.close();
			socketTmp.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * lit un message sur le canal secondaire
	 * @return String : message lu
	 */
	public String getStringCanalDonnee() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader((new Socket(serveurCanalDonnees, portCanalDonnees)).getInputStream()));
			String valRet = "";
			String tempVal;
			while((tempVal = reader.readLine()) != null) {
				valRet += tempVal+"\n";
			}
			return valRet;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * modifie le nom du fichier a renomer
	 * @param fileName String : nom du nouveau fichier a renomer
	 */
	public void setFileToRename(String fileName) {
		this.fileToRename = fileName;
	}

	/**
	 * envoie le nom du fichier a renomer
	 * @return String : nom du fichier a renomer
	 */
	public String getFileToRename() {
		return this.fileToRename;
	}
}


